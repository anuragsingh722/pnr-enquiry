package com.anuragsingh.indianrail;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.anuragsingh.indianrail.adapters.PassengerAdapter;
import com.anuragsingh.indianrail.model.PnrResponse;

public class PnrStatusActivity extends AppCompatActivity {

    TextView pnr_number;
    TextView doj;
    TextView train_number;
    TextView train_name;

    TextView from_station;
    TextView to_station;
    TextView boarding_point;
    TextView reservation_upto;

    TextView seating_class;
    TextView total_passengers;
    TextView chart_prepared;

    ListView passenger_list;

    PnrResponse pnrResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnr_status);

        pnrResponse = (PnrResponse) getIntent().getSerializableExtra("PNR_RESPONSE");
        setTitle("PNR STATUS");

        pnr_number = (TextView) findViewById(R.id.pnr_number);
        doj = (TextView) findViewById(R.id.doj);
        train_number = (TextView) findViewById(R.id.train_number);
        train_name = (TextView) findViewById(R.id.train_name);

        from_station = (TextView) findViewById(R.id.from_station);
        to_station = (TextView) findViewById(R.id.to_station);
        boarding_point = (TextView) findViewById(R.id.boarding_point);
        reservation_upto = (TextView) findViewById(R.id.reservation_upto);

        seating_class = (TextView) findViewById(R.id.seating_class);
        total_passengers = (TextView) findViewById(R.id.total_passengers);
        chart_prepared = (TextView) findViewById(R.id.chart_prepared);

        passenger_list = (ListView) findViewById(R.id.passenger_list);

        /**
         * ASSIGNING VALUES
         */
        pnr_number.setText(pnrResponse.getPnr());
        doj.setText(pnrResponse.getDoj());
        train_number.setText(pnrResponse.getTrain_num());
        train_name.setText(pnrResponse.getTrain_name());
        from_station.setText(pnrResponse.getFrom_station_name());
        to_station.setText(pnrResponse.getTo_station_name());
        boarding_point.setText(pnrResponse.getBoarding_point_name());
        reservation_upto.setText(pnrResponse.getReservation_upto_name());

        seating_class.setText(pnrResponse.getSeat_class());
        total_passengers.setText(pnrResponse.getTotal_passengers());
        chart_prepared.setText(pnrResponse.getChart_prepared());


        PassengerAdapter pa = new PassengerAdapter(this, R.layout.single_passenger_item, pnrResponse.getPassengers());
        passenger_list.setAdapter(pa);
    }
}
