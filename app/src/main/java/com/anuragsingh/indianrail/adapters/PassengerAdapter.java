package com.anuragsingh.indianrail.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.anuragsingh.indianrail.R;
import com.anuragsingh.indianrail.model.Passenger;

import java.util.List;

/**
 * Created by anuragsingh on 07/03/16.
 */
public class PassengerAdapter extends ArrayAdapter {

    Context context;
    List<Passenger> passengers;

    public PassengerAdapter(Context context, int resource, List objects) {
        super(context, resource, objects);

        this.context = context;
        this.passengers = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.single_passenger_item, parent, false);

        TextView no = (TextView) view.findViewById(R.id.no);
        TextView current_status = (TextView) view.findViewById(R.id.current_status);
        TextView coach_position = (TextView) view.findViewById(R.id.coach_position);
        TextView booking_status = (TextView) view.findViewById(R.id.booking_status);

        no.setText(passengers.get(position).getNo());
        current_status.setText(passengers.get(position).getCurrent_status());
        coach_position.setText(passengers.get(position).getCoach_position());
        booking_status.setText(passengers.get(position).getBooking_status());

        return view;
    }

    @Override
    public int getCount() {
        return passengers.size();
    }

    @Override
    public Passenger getItem(int position) {
        return passengers.get(position);
    }
}
