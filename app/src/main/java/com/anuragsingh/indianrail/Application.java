package com.anuragsingh.indianrail;

/**
 * Created by anuragsingh on 06/03/16.
 */
public class Application extends android.app.Application {
    private static Application mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static synchronized Application getInstance() {
        return mInstance;
    }
}
