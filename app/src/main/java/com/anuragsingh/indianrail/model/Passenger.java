package com.anuragsingh.indianrail.model;

import java.io.Serializable;

/**
 * Created by anuragsingh on 06/03/16.
 */
public class Passenger implements Serializable{
    private String no;
    private String booking_status;
    private String current_status;
    private String coach_position;

    public String getNo() {
        return no;
    }

    public Passenger setNo(String no) {
        this.no = no;
        return this;
    }

    public String getBooking_status() {
        return booking_status;
    }

    public Passenger setBooking_status(String booking_status) {
        this.booking_status = booking_status;
        return this;
    }

    public String getCurrent_status() {
        return current_status;
    }

    public Passenger setCurrent_status(String current_status) {
        this.current_status = current_status;
        return this;
    }

    public String getCoach_position() {
        return coach_position;
    }

    public Passenger setCoach_position(String coach_position) {
        this.coach_position = coach_position;
        return this;
    }
}