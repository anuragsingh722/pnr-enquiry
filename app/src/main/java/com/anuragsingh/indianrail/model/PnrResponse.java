package com.anuragsingh.indianrail.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by anuragsingh on 06/03/16.
 */
public class PnrResponse implements Serializable{
    private String response_code;
    private String error;
    private String train_name;
    private String train_num;
    private String pnr;

    private String doj;
    private String chart_prepared;
    private String seat_class;
    private String total_passengers;

    private String train_start_month;
    private String train_start_year;
    private String train_start_day;

    private String from_station_code;
    private String from_station_name;

    private String boarding_point_code;
    private String boarding_point_name;

    private String to_station_code;
    private String to_station_name;

    private String reservation_upto_code;
    private String reservation_upto_name;

    private ArrayList <Passenger> passengers;

    public String getResponse_code() {
        return response_code;
    }

    public PnrResponse setResponse_code(String response_code) {
        this.response_code = response_code;
        return this;
    }

    public String getError() {
        return error;
    }

    public PnrResponse setError(String error) {
        this.error = error;
        return this;
    }

    public String getTrain_name() {
        return train_name;
    }

    public PnrResponse setTrain_name(String train_name) {
        this.train_name = train_name;
        return this;
    }

    public String getTrain_num() {
        return train_num;
    }

    public PnrResponse setTrain_num(String train_num) {
        this.train_num = train_num;
        return this;
    }

    public String getPnr() {
        return pnr;
    }

    public PnrResponse setPnr(String pnr) {
        this.pnr = pnr;
        return this;
    }

    public String getDoj() {
        return doj;
    }

    public PnrResponse setDoj(String doj) {
        this.doj = doj;
        return this;
    }

    public String getChart_prepared() {
        return chart_prepared;
    }

    public PnrResponse setChart_prepared(String chart_prepared) {
        this.chart_prepared = chart_prepared;
        return this;
    }

    public String getSeat_class() {
        return seat_class;
    }

    public PnrResponse setSeat_class(String seat_class) {
        this.seat_class = seat_class;
        return this;
    }

    public String getTotal_passengers() {
        return total_passengers;
    }

    public PnrResponse setTotal_passengers(String total_passengers) {
        this.total_passengers = total_passengers;
        return this;
    }

    public String getTrain_start_month() {
        return train_start_month;
    }

    public PnrResponse setTrain_start_month(String train_start_month) {
        this.train_start_month = train_start_month;
        return this;
    }

    public String getTrain_start_year() {
        return train_start_year;
    }

    public PnrResponse setTrain_start_year(String train_start_year) {
        this.train_start_year = train_start_year;
        return this;
    }

    public String getTrain_start_day() {
        return train_start_day;
    }

    public PnrResponse setTrain_start_day(String train_start_day) {
        this.train_start_day = train_start_day;
        return this;
    }

    public String getFrom_station_code() {
        return from_station_code;
    }

    public PnrResponse setFrom_station_code(String from_station_code) {
        this.from_station_code = from_station_code;
        return this;
    }

    public String getFrom_station_name() {
        return from_station_name;
    }

    public PnrResponse setFrom_station_name(String from_station_name) {
        this.from_station_name = from_station_name;
        return this;
    }

    public String getBoarding_point_code() {
        return boarding_point_code;
    }

    public PnrResponse setBoarding_point_code(String boarding_point_code) {
        this.boarding_point_code = boarding_point_code;
        return this;
    }

    public String getBoarding_point_name() {
        return boarding_point_name;
    }

    public PnrResponse setBoarding_point_name(String boarding_point_name) {
        this.boarding_point_name = boarding_point_name;
        return this;
    }

    public String getTo_station_code() {
        return to_station_code;
    }

    public PnrResponse setTo_station_code(String to_station_code) {
        this.to_station_code = to_station_code;
        return this;
    }

    public String getTo_station_name() {
        return to_station_name;
    }

    public PnrResponse setTo_station_name(String to_station_name) {
        this.to_station_name = to_station_name;
        return this;
    }

    public String getReservation_upto_code() {
        return reservation_upto_code;
    }

    public PnrResponse setReservation_upto_code(String reservation_upto_code) {
        this.reservation_upto_code = reservation_upto_code;
        return this;
    }

    public String getReservation_upto_name() {
        return reservation_upto_name;
    }

    public PnrResponse setReservation_upto_name(String reservation_upto_name) {
        this.reservation_upto_name = reservation_upto_name;
        return this;
    }

    public ArrayList<Passenger> getPassengers() {
        return passengers;
    }

    public PnrResponse setPassengers(ArrayList<Passenger> passengers) {
        this.passengers = passengers;
        return this;
    }
}
