package com.anuragsingh.indianrail;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.anuragsingh.indianrail.model.Constants;
import com.anuragsingh.indianrail.model.Passenger;
import com.anuragsingh.indianrail.model.PnrResponse;
import com.anuragsingh.indianrail.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PnrEnquiryActivity extends AppCompatActivity {

    private EditText pnr_element;
    private String pnr_number;
    private ProgressBar progressBar;
    private ArrayList<Passenger> passengers;
    private PnrResponse pnrResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pnr_enquiry);

        pnr_element = (EditText) findViewById(R.id.pnr_number);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    public void requestStatus(View view) {
        pnr_number = pnr_element.getText().toString();
        progressBar.setVisibility(View.VISIBLE);
        String url = Constants.URL_PNRSTATUS + pnr_number + Constants.API_KEY + "/";
        Log.d("URL", url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject status = new JSONObject(response);
                    passengers = new ArrayList<>();
                    JSONArray passenger_list = status.getJSONArray("passengers");

                    for (int i =0; i< passenger_list.length(); i++){
                        Passenger current = new Passenger();
                        JSONObject cp = passenger_list.getJSONObject(i);
                        current.setNo(cp.getString("no"))
                                .setBooking_status(cp.getString("booking_status"))
                                .setCurrent_status(cp.getString("current_status"))
                                .setCoach_position(cp.getString("coach_position"));
                        passengers.add(current);
                    }

                    pnrResponse = new PnrResponse();
                    pnrResponse.setError(status.getString("error"))
                            .setDoj(status.getString("doj"))
                            .setResponse_code(status.getString("response_code"))
                            .setBoarding_point_code(status.getJSONObject("boarding_point").getString("code"))
                            .setBoarding_point_name(status.getJSONObject("boarding_point").getString("name"))
                            .setReservation_upto_code(status.getJSONObject("reservation_upto").getString("code"))
                            .setReservation_upto_name(status.getJSONObject("reservation_upto").getString("name"))
                            .setSeat_class(status.getString("class"))
                            .setTotal_passengers(String.valueOf(status.getInt("total_passengers")))
                            .setPnr(status.getString("pnr"))
                            .setChart_prepared(status.getString("chart_prepared"))
                            .setFrom_station_code(status.getJSONObject("from_station").getString("code"))
                            .setFrom_station_name(status.getJSONObject("from_station").getString("name"))
                            .setTrain_start_day(String.valueOf(status.getJSONObject("train_start_date").get("day")))
                            .setTrain_start_month(String.valueOf(status.getJSONObject("train_start_date").get("month")))
                            .setTrain_start_year(String.valueOf(status.getJSONObject("train_start_date").get("year")))
                            .setTrain_num(status.getString("train_num"))
                            .setTrain_name(status.getString("train_name"))
                            .setTo_station_code(status.getJSONObject("to_station").getString("code"))
                            .setTo_station_name(status.getJSONObject("to_station").getString("name"))
                            .setPassengers(passengers);


                    if (String.valueOf(status.getInt("response_code")).equals("200")){
                        Intent intent = new Intent(PnrEnquiryActivity.this, PnrStatusActivity.class);
                        intent.putExtra("PNR_RESPONSE", pnrResponse);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("TAG", response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Log.d("TAG", error.toString());
            }
        });

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));

        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent) {
            // Adding request to request queue
            Volley.newRequestQueue(this).add(stringRequest);
        }
        else{
            progressBar.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), "You seem to be disconnected, No Internet", Toast.LENGTH_LONG).show();
        }
    }


}
